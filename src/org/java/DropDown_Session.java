package org.java;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown_Session {

	public static void main(String[] args) throws InterruptedException {

		// configure the browser
		// ChromeBrowser
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\sridhar.elumalai\\Documents\\Internal_Session\\drivers\\chromedriver.exe");

		//Open the Broswer
		WebDriver driver = new ChromeDriver();
		
		//maximize the browser 
		driver.manage().window().maximize();
		
		//navigate to a website 
		driver.get("https://chercher.tech/practice/practice-dropdowns-selenium-webdriver");
		
		//to locate the dropdown present in the website 
		WebElement dropDown1 = driver.findElement(By.xpath("//select[@id='first']"));
		
		//to select a value from a dropdown
		Select select = new Select(dropDown1);
		
		Thread.sleep(3000);
		//Select By visibleText
		select.selectByVisibleText("Google");
		
		Thread.sleep(3000);
		//select by value 
		select.selectByValue("Apple");
		
		
		Thread.sleep(3000);
		//select by index
		//index value starts from 0 to n-1
		
		select.selectByIndex(3); //Yahoo
		
		//Now going forward to Multi_value DropDown
		
		Select select1 = new Select(driver.findElement(By.xpath("//select[@id='second']")));
		
		//Is multiple
		boolean multiple = select1.isMultiple();
		System.out.println("It is multi-value Dropdown "+multiple);
		
		Thread.sleep(3000);
		select1.selectByIndex(0);
		Thread.sleep(3000);
		select1.selectByValue("burger");
		Thread.sleep(3000);
		select1.selectByVisibleText("Donut");
		Thread.sleep(3000);
		
		//Deselect a value from the DropDown
		
		select1.deselectAll();
		Thread.sleep(3000);
		
		
		
		//close
		driver.quit();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
